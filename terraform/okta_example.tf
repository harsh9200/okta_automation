terraform {
    required_providers {
        okta = {
            source = "okta/okta"
            version = "~> 3.10"
        }
    }
}

provider "okta" {
    org_name  = "godpeed-latest"
    base_url  = "okta.com"
    api_token = "00VmTzx6ZgBu2CMcd68EruXg4nubcrcTf5fTiduF7G"
}

# ? Resource to create APP
resource "okta_app_saml" "App" {
    label                    = "App_1"
    auto_submit_toolbar      = false
    hide_ios                 = false
    hide_web                 = false
    sso_url                  = "https://godpeed-latest.com/sso"
    recipient                = "https://godpeed-latest.com/sso"
    destination              = "https://godpeed-latest.com/sso"
    audience                 = "https://godpeed-latest.com/SSO/metadata"
    idp_issuer               = "http://www.okta.com/$${org.externalKey}"
    subject_name_id_template = "$${user.userName}"
    subject_name_id_format   = "urn:oasis:names:tc:SAML:1.1:nameid-format:unspecified"
    response_signed          = true
    request_compressed       = false
    signature_algorithm      = "RSA_SHA256"
    digest_algorithm         = "SHA256"
    honor_force_authn        = true
    authn_context_class_ref  = "urn:oasis:names:tc:SAML:2.0:ac:classes:PasswordProtectedTransport"

    attribute_statements {
        type         = "EXPRESSION"
        name         = "FirstName"
        namespace    = "urn:oasis:names:tc:SAML:2.0:attrname-format:unspecified"
        values       = [ "user.firstName" ]
    }

    attribute_statements {
        type         = "EXPRESSION"
        name         = "LastName"
        namespace    = "urn:oasis:names:tc:SAML:2.0:attrname-format:unspecified"
        values       = [ "user.lastName" ]
    }

    attribute_statements {
        type         = "EXPRESSION"
        name         = "UserName"
        namespace    = "urn:oasis:names:tc:SAML:2.0:attrname-format:unspecified"
        values       = [ "user.login" ]
    }

    attribute_statements {
        type         = "EXPRESSION"
        name         = "Email"
        namespace    = "urn:oasis:names:tc:SAML:2.0:attrname-format:unspecified"
        values       = [ "user.email" ]
    }

    attribute_statements {
        type         = "EXPRESSION"
        name         = "Role"
        namespace    = "urn:oasis:names:tc:SAML:2.0:attrname-format:unspecified"
        values       = [ "appuser.role" ]
    }

    attribute_statements {
        type         = "EXPRESSION"
        name         = "PrimaryAgency"
        namespace    = "urn:oasis:names:tc:SAML:2.0:attrname-format:unspecified"
        values       = [ "appuser.agency_num" ]
    }
}

# ? Ouptut Application Id 
output "application_id" {
    value = okta_app_saml.App.id
}

